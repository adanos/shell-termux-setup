#! /bin/bash
#
# Initially made by https://github.com/sahilsehwag/android-termux-setup-script
#

# Fail on any command error
set -Eeuo pipefail

if [[ "$*" =~ help ]]; then
  echo "all = INSTALLS EVERYTHING"
  echo "bash = SETUP .bashrc"
  echo "zsh = INSTALLS ZSH AND SETUP .zshrc. TO CHANGE DEFAULT SHELL USE chsh AND TYPE zsh"
  echo "ohmyzsh = INSTALLS OH-MY-ZSH"
  echo "vim = INSTALLS vim-python AND SETUP .vimrc"
  echo "storage = ENABLES ACCESS TO PHONE STORAGE"
  echo "upgrade = UPGRADES PACKAGES"
  echo "software = INSTALLS COMMON SOFTWARE SET (SEE SCRIPT CODE)"
  echo "ubuntu = INSTALL UBUNTU 20.04 LTS AS PROOT-DISTRO"
  echo "hotkeys = APPLY KEYBOARD KEYS F1-12"
fi


if [[ "$*" =~ upgrade || "$*" =~ all ]]; then
  apt update -y -o Dpkg::Options::=--force-confnew && \
  apt upgrade -y && \
  apt autoremove -y && \
  apt autoclean && \
  apt clean
fi

if [[ "$*" =~ software || "$*" =~ all ]]; then
  apt install -y \
    man \
    curl \
    termux-exec \
    proot \
    hexcurse \
    sed \
    gawk \
    nano \
    vim \
    mc \
    htop \
    tmux \
    nmap \
    git \
    wget \
    openssh \
    termux-services \
    termux-tools \
    python \
    build-essential \
    cmake \
    rsync \
    termux-api \
    bzip2 \
    findutils \
    gzip \
    grep \
    less \
    tracepath \
    lsof \
    net-tools \
    nmap-ncat \
    tar \
    unzip \
    zip \
    util-linux \
    xz-utils \
    bash-completion \
    jq \
    bc \
    httping \
    dnsutils \
    nano

    pip install --upgrade pip setuptools
fi

#BASH
if [[ "$*" =~ bash || "$*" =~ all ]]; then
  touch ~/.bashrc
  if [[ -z "$(cat ~/.bashrc | grep 'alias ll' )" ]]; then
    {
      echo "alias ll='ls -l'"
    } >> ~/.bashrc
  fi
fi

#ZSH
if [[ "$*" =~ zsh || "$*" =~ all ]]; then
  pkg install -y zsh
  touch ~/.zshrc
  if [[ -z "$(cat ~/.zshrc | grep 'alias ll' )" ]]; then
    {
      echo "alias ll='ls -l'"
    } >> ~/.zshrc
  fi
fi

#VIM
if [[ "$*" =~ vim || "$*" =~ all ]]; then
  pkg install -y vim
  if [[ ! -f ~/.vimrc ]]; then
    touch ~/.vimrc
    {
      echo '"PREFERENCES'
      echo 'set tabstop=4'
      echo 'set shiftwidth=4'
      echo 'set autoindent'
      echo 'set ignorecase'
      echo 'set smartcase'
      echo 'set nobackup'
      echo 'set noswapfile'
      echo 'set number'
      echo 'set relativenumber'
      echo 'set incsearch'
      echo 'set hlsearch'
      echo 'set nowrap'
      echo ''

      echo '"LEADER MAPPINGS'
      echo 'let mapleader=" "'
      echo 'let maplocalleader=","'
      echo 'nnoremap <LEADER><LEADER> :'
      echo ''

      echo '"BUFFER MAPPINGS'
      echo 'nnoremap <Leader>ba :badd '
      echo 'nnoremap <Leader>be :edit '
      echo 'nnoremap <Leader>bd :bdelete<CR>'
      echo 'nnoremap <Leader>bw :w<CR>'
      echo 'nnoremap <Leader>bfw :w!<CR>'
      echo 'nnoremap <Leader>bn :bnext<CR>'
      echo 'nnoremap <Leader>bp :bprevious<CR>'
      echo ""

      echo '"WINDOWS MAPPINGS'
      echo 'nnoremap <Leader>wo :only<CR>'
      echo 'nnoremap <Leader>ws :split '
      echo 'nnoremap <Leader>wv :vsplit '
      echo 'nnoremap <Leader>wj <C-W><C-J>'
      echo 'nnoremap <Leader>wk <C-W><C-K>'
      echo 'nnoremap <Leader>wl <C-W><C-L>'
      echo 'nnoremap <Leader>wh <C-W><C-H>'
      echo ""

      echo '"TAB MAPPINGS'
      echo 'nnoremap <LEADER>ta :tabnew<CR>     '
      echo 'nnoremap <LEADER>tc :tabclose<CR>   '
      echo 'nnoremap <LEADER>tn :tabnext<CR>    '
      echo 'nnoremap <LEADER>tp :tabprevious<CR>'
      echo 'nnoremap <LEADER>th :tabmove -<CR>  '
      echo 'nnoremap <LEADER>tl :tabmove +<CR>  '
      echo ""

      echo '"VIM MAPPINGS'
      echo 'nnoremap <Leader>vrc :edit ~/.vimrc<CR>'
      echo 'nnoremap <Leader>vs :source ~/.vimrc<CR>'
      echo 'nnoremap <Leader>vq :q<CR>'
      echo 'nnoremap <Leader>vfq :q!<CR>'
      echo 'nnoremap <Leader>vc :normal! 0i"<Esc>'
      echo 'nnoremap <Leader>vu :normal! 0x'
    } >> ~/.vimrc
  fi
fi

#OH-MY-ZSH
if [[ "$*" =~ ohmyzsh || "$*" =~ all ]]; then
  if [[ ! -d ~/.oh-my-zsh ]]; then
    pkg install -y curl
    pkg install -y git
    bash -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
  fi
fi

#STORAGE
if [[ "$*" =~ storage || "$*" =~ all ]]; then
  if [[ ! -d ~/storage ]]; then
    termux-setup-storage
  fi
fi

#HOTKEYS
if [[ "$*" =~ hotkeys || "$*" =~ all ]]; then
  if [[ ! -d ~/.termux ]]; then
    mkdir .termux
  fi
  touch ~/.termux/termux.properties

  if [[ -z "$(cat ~/.termux/termux.properties | grep '^extra-keys')" ]]; then
    {
      echo "extra-keys = [ \\
 ['F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12' ], \\
 ['ESC','|','/','HOME','UP','END','PGUP','DEL'], \\
 ['TAB','CTRL','ALT','LEFT','DOWN','RIGHT','PGDN','BKSP'] \\
]"
    } >> ~/.termux/termux.properties
    termux-reload-settings
  fi
fi

#UBUNTU
if [[ "$*" =~ ubuntu || "$*" =~ all ]]; then
  pkg install -y proot-distro
  if [[ -z "$(proot-distro list 2>&1 | grep -A 1 'ubuntu' | grep 'installed' | grep -v 'NOT')" ]]; then
    proot-distro install ubuntu-lts --override-alias ubuntu-t
  fi
  if [[ -z "$(cat ~/.bashrc | grep 'alias ubuntu')" ]]; then
    touch ~/.bashrc
    {
      echo "alias ubuntu='proot-distro login --fix-low-ports --termux-home --shared-tmp --bind ~/storage:/mnt/storage --bind /mnt:/mnt/host_mnt ubuntu-t'"
    } >> ~/.bashrc
  fi
fi

echo "Run: source \$PREFIX/etc/profile"
echo "To apply alias changes"
