# Termix Setup Scripts

- `setup.sh` - is a shell script to install and setup various packages in Termux terminal for Android.
It can setup **bash, zsh, vim** and few other tools. It writes reasonable configuration files.<br>

**_Note_**: Please make sure that configuration files like .zshrc/.bashrc/.vimrc etc are either not present or are empty to ensure proper working.

Script is pseudo-idempotent, so at least, config files won't be modified once configured ot in case if conflicts.
If an update should be force executed - please address such cases manually.

It's highly recommended to [install Termux from F-Droid](https://wiki.termux.com/wiki/Installing_from_F-Droid) store with all plugins.

**Tip:** If you removed Termux (installed previously from Play Store) and get a signature error in F-Droid - wipe F-Droid data and try again.

# Usage

Download the script and execute it: `bash setup.sh <ARGUMENT>`
Or just use latest version: `pkg install -y curl && curl -sL https://gitlab.com/adanos/shell-termux-setup/-/raw/master/termux-setup.sh | bash -s all`
Or (to be short): `pkg install -y curl && curl -sL https://clck.ru/UPPQW | bash -s all`

**NOTE**: Avoid using `bash termux.sh all` option, due to some bug, we have to run this command multiple times to get all the stuff installed and configured. I would suggest installing using individual options instead of `all` right now.

## Help

`bash termux.sh help`

## Ubuntu: Install VSCode Server

From `code-server` install [doc](https://github.com/cdr/code-server):

```

curl -fsSL https://code-server.dev/install.sh | sh
mkdir /var/lib/code-server
mkdir /usr/local/lib/code-server
mkdir /var/log/code-server
code-server --auth none --cert-host localhost --disable-telemetry --bind-addr 127.0.0.1:13333 --user-data-dir /var/lib/code-server --extensions-dir /usr/local/lib/code-server | tee /var/log/code-server/log
```

Also, you can setup a script with execution command above at `/usr/local/bin` in Ubuntu env and at `~/.termux/boot` at termux env to start server on device boot.
Please check [Termux Boot docs](https://wiki.termux.com/wiki/Termux:Boot) to get additional details about autorun.

Or, you can use [termux-only install](https://github.com/cdr/code-server/blob/main/docs/install.md#termux) without ubuntu env

## Ubuntu: Bootstrap user

```
apt update && apt install -y sudo
sudo useradd --comment "Full Name" \
             --groups adm,dialout,cdrom,floppy,sudo,audio,dip,video,plugdev \
             --create-home \
             --shell /bin/bash \
             --user-group \
             username

sudo passwd username

cat << SUDOERS_EOF >  /etc/sudoers.d/90-admin-user
# User rules
username ALL=(ALL) NOPASSWD:ALL

SUDOERS_EOF

```

And update `ubuntu` alias to login with user created

# List of Arguments

> help    = shows the list of all valid arguments and their description
> all     = installs everything
> bash    = setup .bashrc
> zsh     = installs zsh and setup .zshrc. To change default shell use "chsh" and type "zsh"
> ohmyzsh = installs oh-my-zsh
> vim     = installs vim-python and setup .vimrc
> storage = enables access to phone storage
> upgrade = upgrades all packages
> softeare = install hardcoded software set
> ubuntu = install proot-distro, Ubuntu 20.04, and 'ubuntu' shell alias
> hotkeys = updates termux to use
